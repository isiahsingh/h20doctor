﻿using H20.Data.Helpers;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.Data.PoolRepo
{
    public class PoolRepo
    {
        public Response<List<Pool>> GetAllPools()
        {
            Response<List<Pool>> response = new Response<List<Pool>>();
            DatabaseHelper dbHelper = new DatabaseHelper();
            try
            {
                string query = "SELECT * FROM POOLS";
                List<Pool> pools = dbHelper.QueryDatabaseForAll<Pool>(query);
                if (pools != null)
                {
                    response.Data = pools;
                    response.IsSuccessful = true;
                }
                else
                {
                    response.Data = pools;
                    response.IsSuccessful = true;
                    response.Message = "No Pools returned.";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<int> Add(Pool pool)
        { 
            Response<int> response = new Response<int>();
            DatabaseHelper dbHelper = new DatabaseHelper();
            try
            {
                string query = @"INSERT INTO POOLS(CustomerID, [Address], Street, City, [State], Zip, PoolType, AdditionalInformation, [DayOfWeek])
                                VALUES(@CustomerId, @Address, @Street, @City, @State, @Zip, @PoolType, @AdditionalInformation, @DayOfWeek)";
                var result = dbHelper.ExecuteNonQuery<Pool>(query, pool);
                if (result != null && (int)result > 0) //result is 1
                {
                    response.Data = (int)result;
                    response.IsSuccessful = true;
                }
                else
                {
                    response.Data = (int)result;
                    response.IsSuccessful = true;
                    response.Message = "Error adding pool.";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
