﻿using Dapper;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.Data.Helpers
{
    public class DatabaseHelper
    {
        private IDbConnection _db;
        public DatabaseHelper()
        {
            _db = new SqlConnection(ConfigurationManager.ConnectionStrings["H20DB"].ConnectionString);
        }
        public List<T> QueryDatabaseForAll<T>(string query)
        {
            List<T> allData = _db.Query<T>(query).ToList();
            return allData;
        }

        public object ExecuteNonQuery<T>(string query, T itemToAdd)
        {
            var result = _db.Execute(query, itemToAdd);

            return result;
        }

    }
}
