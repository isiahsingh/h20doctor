﻿using H20.Data.Helpers;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.Data.AppointmentRepo
{
    public class AppointmentRepo
    {
        public Response<List<Appointment>> GetAll()
        {
            Response<List<Appointment>> response = new Response<List<Appointment>>();
            DatabaseHelper dbHelper = new DatabaseHelper();
            string query = "SELECT * FROM APPOINTMENTS";
            try
            {
                List<Appointment> appointments = dbHelper.QueryDatabaseForAll<Appointment>(query);
                if (appointments != null)
                {
                    response.Data = appointments;
                    response.IsSuccessful = true;
                }
                else
                {
                    response.Data = appointments;
                    response.IsSuccessful = true;
                    response.Message = "No appointments returned.";
                }
            }
            catch(Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
