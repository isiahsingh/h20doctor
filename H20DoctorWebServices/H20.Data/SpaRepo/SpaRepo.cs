﻿using H20.Data.Helpers;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.Data.SpaRepo
{
    public class SpaRepo
    {
        public Response<List<Spa>> GetAllSpas()
        {
            Response<List<Spa>> response = new Response<List<Spa>>();
            DatabaseHelper dbHelper = new DatabaseHelper();
            try
            {
                string query = "SELECT * FROM SPAS";
                List<Spa> spas = dbHelper.QueryDatabaseForAll<Spa>(query);
                if (spas != null)
                {
                    response.Data = spas;
                    response.IsSuccessful = true;
                }
                else
                {
                    response.Data = spas;
                    response.IsSuccessful = true;
                    response.Message = "No Spas returned.";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<int> Add(Spa spa)
        {
            Response<int> response = new Response<int>();
            DatabaseHelper dbHelper = new DatabaseHelper();
            try
            {
                string query = @"INSERT INTO SPAS(CustomerID, [Address], Street, City, [State], Zip, SpaType, AdditionalInformation, [DayOfWeek])
                                VALUES(@CustomerId, @Address, @Street, @City, @State, @Zip, @SpaType, @AdditionalInformation, @DayOfWeek)";
                var result = dbHelper.ExecuteNonQuery<Spa>(query, spa);
                if (result != null && (int)result > 0) //result is 1
                {
                    response.Data = (int)result;
                    response.IsSuccessful = true;
                }
                else
                {
                    response.Data = (int)result;
                    response.IsSuccessful = true;
                    response.Message = "Error adding Spa.";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
