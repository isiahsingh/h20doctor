﻿using H20.Data.Helpers;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.Data.CustomerRepo
{
    public class CustomerRepo
    {
        public Response<List<Customer>> GetAllCustomers()
        {
            Response<List<Customer>> response = new Response<List<Customer>>();
            DatabaseHelper dbHelper = new DatabaseHelper();
            try
            {
                string query = "SELECT * FROM CUSTOMERS";
                List<Customer> customers = dbHelper.QueryDatabaseForAll<Customer>(query);
                if (customers != null)
                {
                    response.Data = customers;
                    response.IsSuccessful = true;
                }
                else
                {
                    response.Data = customers;
                    response.IsSuccessful = true;
                    response.Message = "No Customers returned.";
                }
            }
            catch(Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<int> AddCustomer(Customer custToAdd)
        {
            Response<int> response = new Response<int>();
            DatabaseHelper dbHelper = new DatabaseHelper();
            try
            {
                string query = @"INSERT INTO Customers(Phone, [Address], [Street], City, [State], Zip, Email, FirstName, LastName, MonthlyRate)
                                VALUES(@Phone, @Address, @Street, @City, @State, @Zip, @Email, @FirstName, @LastName, @MonthlyRate)";
                var result = dbHelper.ExecuteNonQuery<Customer>(query, custToAdd);
                if (result != null && (int)result > 0) //result is 1
                {
                    response.Data = (int)result;
                    response.IsSuccessful = true;
                }
                else
                {
                    response.Data = (int)result;
                    response.IsSuccessful = true;
                    response.Message = "No Customers returned.";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<int> UpdateCustomer(Customer custToUpdate)
        {
            Response<int> response = new Response<int>();
            DatabaseHelper dbHelper = new DatabaseHelper();

            try
            {
                string query = @"UPDATE Customers
                                SET Phone = @Phone, [Address] = @Address, [Street] = @Street, City = @City, [State] = @State, Zip = @Zip, Email = @Email, FirstName = @FirstName, LastName = @LastName, MonthlyRate = @MonthlyRate
                                WHERE Id = @Id";
                var result = dbHelper.ExecuteNonQuery<Customer>(query, custToUpdate);
                if (result != null && (int)result > 0) 
                {
                    response.Data = (int)result;
                    response.IsSuccessful = true;
                }
                else
                {
                    response.Data = (int)result;
                    response.IsSuccessful = true;
                    response.Message = "No Customers returned.";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<int> DeleteCustomer(Customer custToDelete)
        {
            Response<int> response = new Response<int>();
            DatabaseHelper dbHelper = new DatabaseHelper();

            try
            {
                string query = @"DELETE FROM CUSTOMERS WHERE Id = @Id";
                var result = dbHelper.ExecuteNonQuery<Customer>(query, custToDelete);
                if (result != null && (int)result > 0)
                {
                    response.Data = (int)result;
                    response.IsSuccessful = true;
                }
                else
                {
                    response.Data = (int)result;
                    response.IsSuccessful = true;
                    response.Message = "No Customers returned.";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = ex.Message;
            }

            return response;
        }


    }
}
