﻿using H20.Data.Helpers;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.Data.EmployeeRepo
{
    public class EmployeeRepo
    {
        public Response<List<Employee>> GetAll()
        {
            Response<List<Employee>> response = new Response<List<Employee>>();
            DatabaseHelper dbHelper = new DatabaseHelper();
            string query = "SELECT * FROM EMPLOYEES";
            try
            {
                List<Employee> employees = dbHelper.QueryDatabaseForAll<Employee>(query);
                if (employees != null)
                {
                    response.Data = employees;
                    response.IsSuccessful = true;
                }
                else
                {
                    response.Data = employees;
                    response.IsSuccessful = true;
                    response.Message = "No employees returned.";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
