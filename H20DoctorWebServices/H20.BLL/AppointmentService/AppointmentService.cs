﻿using H20.Data.AppointmentRepo;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.BLL.AppointmentService
{
    public class AppointmentService
    {
        public Response<List<Appointment>> GetAll()
        {
            AppointmentRepo appointmentRepo = new AppointmentRepo();
            return appointmentRepo.GetAll();
        }
    }
}
