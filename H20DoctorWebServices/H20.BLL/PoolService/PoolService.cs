﻿using H20.Data.PoolRepo;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.BLL.PoolService
{
    public class PoolService
    {
        public Response<List<Pool>> GetAll()
        {
            PoolRepo poolRepo = new PoolRepo();
            return poolRepo.GetAllPools();
        }

        public Response<int> Add(Pool pool)
        {
            PoolRepo poolRepo = new PoolRepo();
            return poolRepo.Add(pool);
        }
      

    }
}
