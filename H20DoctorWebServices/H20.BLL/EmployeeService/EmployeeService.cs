﻿using H20.Data.EmployeeRepo;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.BLL.EmployeeService
{
    public class EmployeeService
    {
        public Response<List<Employee>> GetAll()
        {
            EmployeeRepo employeeRepo = new EmployeeRepo();
            return employeeRepo.GetAll();
        }
    }
}
