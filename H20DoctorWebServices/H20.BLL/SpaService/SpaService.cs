﻿using H20.Data.SpaRepo;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.BLL.SpaService
{
    public class SpaService
    {
        public Response<List<Spa>> GetAll()
        {
            SpaRepo spaRepo = new SpaRepo();
            return spaRepo.GetAllSpas();
        }

        public Response<int> Add(Spa spa)
        {
            SpaRepo spaRepo = new SpaRepo();
            return spaRepo.Add(spa);
        }
    }
}
