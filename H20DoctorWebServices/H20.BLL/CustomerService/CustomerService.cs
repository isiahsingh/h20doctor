﻿using H20.Data.CustomerRepo;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.BLL.CustomerService
{
    public class CustomerService
    {
        public Response<List<Customer>> GetAllCustomers()
        {
            CustomerRepo customerRepo = new CustomerRepo();
            return customerRepo.GetAllCustomers();
        }

        public Response<int> AddCustomer(Customer custToAdd)
        {
            CustomerRepo customerRepo = new CustomerRepo();
            return customerRepo.AddCustomer(custToAdd);
        }

        public Response<int> UpdateCustomer(Customer custToUpdate)
        {
            CustomerRepo customerRepo = new CustomerRepo();
            return customerRepo.UpdateCustomer(custToUpdate);
        }

        public Response<int> DeleteCustomer(Customer custToDelete)
        {
            CustomerRepo customerRepo = new CustomerRepo();
            return customerRepo.DeleteCustomer(custToDelete);
        }
    }
}
