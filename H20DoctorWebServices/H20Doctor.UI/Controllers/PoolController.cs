﻿using H20.BLL.PoolService;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace H20Doctor.UI.Controllers
{
    [RoutePrefix("api/pool")]
    public class PoolController : ApiController
    {
        [HttpGet]
        [Route("getAll")]
        public IHttpActionResult GetAll()
        {
            PoolService poolService = new PoolService();
            Response<List<Pool>> response = poolService.GetAll();

            if (response.IsSuccessful)
            {
                if (response.Data != null)
                {
                    return Ok(response.Data);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("add")]
        public IHttpActionResult Add(Pool pool)
        {
            try
            {
                PoolService poolService = new PoolService();
                Response<int> response = poolService.Add(pool);
                if (response.Data > 0)
                    return Ok();
                else
                    return BadRequest();
            }
            catch
            {
                return InternalServerError();
            }
        }
    }
}
