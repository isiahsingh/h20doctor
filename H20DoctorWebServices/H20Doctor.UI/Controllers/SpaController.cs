﻿using H20.BLL.SpaService;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace H20Doctor.UI.Controllers
{
    [RoutePrefix("api/spa")]
    public class SpaController : ApiController
    {
        [HttpGet]
        [Route("getAll")]
        public IHttpActionResult GetAll()
        {
            SpaService spaService = new SpaService();
            Response<List<Spa>> response = spaService.GetAll();

            if (response.IsSuccessful)
            {
                if (response.Data != null)
                {
                    return Ok(response.Data);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("add")]
        public IHttpActionResult Add(Spa spa)
        {
            try
            {
                SpaService spaService = new SpaService();
                Response<int> response = spaService.Add(spa);
                if (response.Data > 0)
                    return Ok();
                else
                    return BadRequest();
            }
            catch
            {
                return InternalServerError();
            }
        }
    }
}
