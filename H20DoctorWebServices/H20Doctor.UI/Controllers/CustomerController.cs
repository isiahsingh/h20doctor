﻿using H20.BLL.CustomerService;
using H20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace H20Doctor.UI.Controllers
{
    [RoutePrefix("api/customer")]
    public class CustomerController : ApiController
    {
        public IHttpActionResult GetAll()
        {
            CustomerService customerService = new CustomerService();
            Response<List<Customer>> response = customerService.GetAllCustomers();

            if (response.IsSuccessful)
            {
                if(response.Data != null)
                {
                    return Ok(response.Data);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("add")]
        public IHttpActionResult Add(Customer custToAdd)
        {
            try
            {
                CustomerService custService = new CustomerService();
                Response<int> response = custService.AddCustomer(custToAdd);
                if(response.Data == 1)
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpPut]
        [Route("update")]
        public IHttpActionResult Update(Customer custToUpdate)
        {
            try
            {
                CustomerService custService = new CustomerService();
                Response<int> response = custService.UpdateCustomer(custToUpdate);
                if (response.Data == 1)
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch
            {
                return InternalServerError();
            }
        }


        [HttpPost]
        [Route("delete")]
        public IHttpActionResult Delete(Customer custToDelete)
        {
            try
            {
                CustomerService custService = new CustomerService();
                Response<int> response = custService.DeleteCustomer(custToDelete);
                if (response.Data == 1)
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch
            {
                return InternalServerError();
            }
        }
    }
}
