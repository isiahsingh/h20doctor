﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.Models
{
    public class Appointment
    {
        public int Id { get; set; }
        public Pool Pool { get; set;}
        public Spa Spa { get; set; }
        public string Status { get; set; }
        public DateTime Date { get; set; }
        public Employee Employee { get; set; }
        public string Notes { get; set; }
        public bool Brushed { get; set; }
        public bool Chlorine { get; set; }


    }
}
