﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.Models
{
    public class Customer
    {
        public int? Id { get; set; }
        public string AspNetUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public decimal MonthlyRate { get; set; }

        public List<Pool> Pools { get; set; }
        public List<Spa> Spas { get; set; }

        public List<Appointment> Appointments { get; set; }
    }
}
