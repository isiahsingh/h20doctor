﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H20.Models
{
    public class Pool
    {
        public int? Id { get; set; }
        public int? CustomerId { get; set; }

        public string Address { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }
        public string PoolType { get; set; }
        public string DayOfWeek { get; set; }
        public string AdditionalInformation { get; set; }
    }
}
